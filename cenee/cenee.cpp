/******************************************************************************
 * Cerulean 2 by Joey Robert                                                  *
 ******************************************************************************/
#include "cenee.h"

/******************************************************************************
 * Set bit in value at index, generalized for int types.                      *
 ******************************************************************************/
template <class INTTYPE> inline void setbit(INTTYPE &value, int index)
{
	value |= ((INTTYPE)1 << index);
}

/******************************************************************************
 * Population count                                                           *
 * Returns count of 1 digits in a binary number                               *
 * e.g. x=1101001b returns 4                                                  *
 ******************************************************************************/
inline int popcnt(uint64_t x)
{
#ifdef CMP_INTRINSICS
    return (int)__popcnt64(x);
#else
    x =  x - ((x >> 1) & U64(0x5555555555555555));
    x = (x & U64(0x3333333333333333)) + ((x >> 2) & U64(0x3333333333333333));
    x = (x + (x >> 4)) & U64(0x0f0f0f0f0f0f0f0f);
    x = (x * U64(0x0101010101010101)) >> 56;
    return (int)x;
#endif
}

/******************************************************************************
 * Bitscan Reverse (MSB)                                                      *
 * Returns index (0 = LSB, 63 = MSB) of largest 1                             *
 * e.g. x=0001001b returns 3                                                  *
 ******************************************************************************/
inline int bsr(uint64_t x)
{
    assert(x != 0);
#ifdef CMP_INTRINSICS
    return (int)(__lzcnt64(x)^63);
	
    //unsigned long index;
    //_BitScanReverse64(&index, x);
    //return (int)index;
#else
    int result = 0;
    if(x > 0xFFFFFFFF)                            /* 2**32-1 */
    {
        x >>= 32;
        result = 32;
    }
    if(x > 0xFFFF)                                /* 2**16-1 */
    {
        x >>= 16;
        result += 16;
    }
    if(x > 0xFF)                                  /* 2**8-1 */
    {
        x >>= 8;
        result += 8;
    }
    if(x > 0xF)                                   /* 2**4-1 */
    {
        x >>= 4;
        result += 4;
    }
    if(x > 3)                                     /* 2**2-1 */
    {
        x >>= 2;
        result += 2;
    }
    if(x > 1)                                     /* 2**1-1 */
    {
        x >>= 1;
        result += 1;
    }
    return result;
#endif
}

/******************************************************************************
 * Bitscan Forward (LSB)                                                      *
 * Returns index (0 = LSB, 63 = MSB) of smallest 1                            *
 * e.g. x=0001010b returns 1                                                  *
 * Non-intrinsic implementation, using de Bruijn sequences by                 *
 * Martin Läuter, Charles E. Leiserson, Harald Prokop, Keith H. Randall       *
 ******************************************************************************/
inline int bsf(uint64_t x)
{
    assert(x != 0);
#ifdef CMP_INTRINSICS
    unsigned long index;
    _BitScanForward64(&index, x);
    return (int)index;
#else
    return bsf_index[((x & -x) * bsf_magic) >> 58];
#endif
}

/******************************************************************************
 * On bits                                                                    *
 * Returns vector of indices of on bits                                       *
 * e.g. 10110 returns 1, 2, 4                                                 *
 ******************************************************************************/
void on_bits(vector<int> &on_bits_index, Bitboard x)
{
    assert(x != 0);

    int bit_index, i = 0;
    int size = popcnt(x);
    on_bits_index.resize(size);

    while(x != 0)
    {
        bit_index = bsf(x);
        x ^= (Bitboard)1<<bit_index;
        on_bits_index[i] = bit_index;
        ++i;
    }
}

/******************************************************************************
 * Bit variations                                                             *
 * Populates bit_var vector to contain all Bitboard variations                *
 * e.g. x = 10110                                                             *
 * bitvar = [10110 00110 10100 10010 10000 00100 00010] (00000 not included)  *
 * Size of bit_var is 2**popcnt(x)-1                                          *
 ******************************************************************************/
void bit_variations(vector<Bitboard> &bit_var, Bitboard x)
{
    assert(x != 0);

    Bitboard variation;
    int i, j;
    int size = popcnt(x);
    int var_count = 1<<size;
    vector<int> large_bit_index, small_bit_index;
    on_bits(large_bit_index, x);

    bit_var.resize(var_count-1);

    /* count from 1 to 2**size (e.g. up to 1111 for 4 bits) */
    for(i = 1; i < var_count; ++i)
    {
        variation = 0;
        on_bits(small_bit_index, i);

        /* loop over bits in i and OR large_bit_index[bit_small_bit] together */
        for(j = 0; j < small_bit_index.size(); ++j)
        {
            variation |= (Bitboard)1<<large_bit_index[small_bit_index[j]];
        }

        bit_var[i-1] = variation;
    }
}

/******************************************************************************
 * Attack set                                                                 *
 * Populates attset to contain attack set for each occupancy variation        *
 * Size of result is the same as the input                                    *
 ******************************************************************************/
void attack_sets(vector<Bitboard> &attset, vector<Bitboard> &bitvar, int index, bool is_rook)
{
    assert(index>=A1 && index<=H8);
    int i, j;
    Bitboard attack;
	size_t bv_size = bitvar.size();
    attset.resize(bv_size);

    for(i = 0; i < bv_size; ++i)
    {
        attack = 0;

        if(is_rook)
        {
            for (j=index+8; j <= 55 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j+=8);
            if (j >= 0 && j <= 63) attack |= (Bitboard)(1<<j);

            for (j=index-8; j >= 8 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j-=8);
            if (j >= 0 && j <= 63) attack |= (Bitboard)(1<<j);

            for (j=index+1; j%8 != 7 && j%8 != 0 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j++);
            if (j >= 0 && j <= 63) attack |= (Bitboard)(1<<j);

            for (j=index-1; j%8 != 7 && j%8 != 0 && j >= 0 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j--);
            if (j >= 0 && j <= 63) attack |= (Bitboard)(1<<j);
        } else
        {
            for (j=index+9; j%8 != 7 && j%8 != 0 && j <= 55 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j+=9);
            if (j>=0 && j<=63) attack |= (Bitboard)(1<<j);

            for (j=index-9; j%8 != 7 && j%8 != 0 && j >= 8 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j-=9);
            if (j>=0 && j<=63) attack |= (Bitboard)(1<<j);

            for (j=index+7; j%8 != 7 && j%8 != 0 && j <= 55 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j+=7);
            if (j>=0 && j<=63) attack |= (Bitboard)(1<<j);

            for (j=index-7; j%8 != 7 && j%8 != 0 && j >= 8 && (bitvar[i] & (Bitboard)(1<<j)) == 0; j-=7);
            if (j>=0 && j<=63) attack |= (Bitboard)(1<<j);
        }

        attset[i] = attack;
    }
}

/******************************************************************************
 * King move                                                                  *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 0                                                        *
 * 7   0 0 0 0 0 0 0 0                                                        *
 * 6   0 0 0 0 0 0 0 0                                                        *
 * 5   0 0 1 1 1 0 0 0                                                        *
 * 4   0 0 1 X 1 0 0 0                                                        *
 * 3   0 0 1 1 1 0 0 0                                                        *
 * 2   0 0 0 0 0 0 0 0                                                        *
 * 1   0 0 0 0 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard king_moves(int rank, int file)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);

    Bitboard bb = 0;
    if(rank+1<8) bb ^= (Bitboard)1 << (8*(rank+1) + file);
    if(rank-1>=0) bb ^= (Bitboard)1 << (8*(rank-1) + file);
    if(file+1<8) bb ^= (Bitboard)1 << (8*rank + file+1);
    if(file-1>=0) bb ^= (Bitboard)1 << (8*rank + file-1);
    if(rank+1<8 && file+1<8) bb ^= (Bitboard)1 << (8*(rank+1) + file+1);
    if(rank+1<8 && file-1>=0) bb ^= (Bitboard)1 << (8*(rank+1) + file-1);
    if(rank-1>=0 && file+1<8) bb ^= (Bitboard)1 << (8*(rank-1) + file+1);
    if(rank-1>=0 && file-1>=0) bb ^= (Bitboard)1 << (8*(rank-1) + file-1);
    return bb;
}

/******************************************************************************
 * Knight move                                                                *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 0                                                        *
 * 7   0 0 0 0 0 0 0 0                                                        *
 * 6   0 0 1 0 1 0 0 0                                                        *
 * 5   0 1 0 0 0 1 0 0                                                        *
 * 4   0 0 0 X 0 0 0 0                                                        *
 * 3   0 1 0 0 0 1 0 0                                                        *
 * 2   0 0 1 0 1 0 0 0                                                        *
 * 1   0 0 0 0 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard knight_moves(int rank, int file)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);

    Bitboard bb = 0;
    if(rank+2<8 && file+1<8) bb ^= (Bitboard)1 << (8*(rank+2) + file+1);
    if(rank+2<8 && file-1>=0) bb ^= (Bitboard)1 << (8*(rank+2) + file-1);
    if(rank-2>=0 && file+1<8) bb ^= (Bitboard)1 << (8*(rank-2) + file+1);
    if(rank-2>=0 && file-1>=0) bb ^= (Bitboard)1 << (8*(rank-2) + file-1);
    if(rank+1<8 && file+2<8) bb ^= (Bitboard)1 << (8*(rank+1) + file+2);
    if(rank-1>=0 && file+2<8) bb ^= (Bitboard)1 << (8*(rank-1) + file+2);
    if(rank+1<8 && file-2>=0) bb ^= (Bitboard)1 << (8*(rank+1) + file-2);
    if(rank-1>=0 && file-2>=0) bb ^= (Bitboard)1 << (8*(rank-1) + file-2);
    return bb;
}

/******************************************************************************
 * Single pawn move                                                           *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 0                                                        *
 * 7   0 0 0 0 0 0 0 0                                                        *
 * 6   0 0 0 0 0 0 0 0                                                        *
 * 5   0 0 0 1 0 0 0 0                                                        *
 * 4   0 0 0 X 0 0 0 0                                                        *
 * 3   0 0 0 0 0 0 0 0                                                        *
 * 2   0 0 0 0 0 0 0 0                                                        *
 * 1   0 0 0 0 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard pawn_single_moves(int rank, int file, int side)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);
    assert(side==0 || side==1);

    Bitboard bb = 0;
    if(side == 0 && rank+1<8) bb ^= (Bitboard)1 << (8*(rank+1) + file);
    if(side == 1 && rank-1>=0) bb ^= (Bitboard)1 << (8*(rank-1) + file);
    return bb;
}

/******************************************************************************
 * Double pawn move                                                           *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 0                                                        *
 * 7   0 0 0 0 0 0 0 0                                                        *
 * 6   0 0 0 0 0 0 0 0                                                        *
 * 5   0 0 0 0 0 0 0 0                                                        *
 * 4   0 0 0 1 0 0 0 0                                                        *
 * 3   0 0 0 0 0 0 0 0                                                        *
 * 2   0 0 0 X 0 0 0 0                                                        *
 * 1   0 0 0 0 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard pawn_double_moves(int rank, int file, int side)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);
    assert(side==0 || side==1);

    Bitboard bb = 0;
    if(side == 0 && rank == 1) bb ^= (Bitboard)1 << (8*(rank+2) + file);
    if(side == 1 && rank == 6) bb ^= (Bitboard)1 << (8*(rank-2) + file);
    return bb;
}

/******************************************************************************
 * Pawn captures                                                              *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 0                                                        *
 * 7   0 0 0 0 0 0 0 0                                                        *
 * 6   0 0 0 0 0 0 0 0                                                        *
 * 5   0 0 1 0 1 0 0 0                                                        *
 * 4   0 0 0 X 0 0 0 0                                                        *
 * 3   0 0 0 0 0 0 0 0                                                        *
 * 2   0 0 0 0 0 0 0 0                                                        *
 * 1   0 0 0 0 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard pawn_capture_moves(int rank, int file, int side)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);
    assert(side==0 || side==1);

    Bitboard bb = 0;
    if(side == 0 && rank+1<8)
    {
        if(file+1<8) bb ^= (Bitboard)1 << (8*(rank+1) + file+1);
        if(file-1>=0) bb ^= (Bitboard)1 << (8*(rank+1) + file-1);
    }
    if(side == 1 && rank-1>=0)
    {
        if(file+1<8) bb ^= (Bitboard)1 << (8*(rank-1) + file+1);
        if(file-1>=0) bb ^= (Bitboard)1 << (8*(rank-1) + file-1);
    }
    return bb;
}

/******************************************************************************
 * Rook mask, includes edges                                                  *
 *                                                                            *
 * 8   0 0 0 1 0 0 0 0                                                        *
 * 7   0 0 0 1 0 0 0 0                                                        *
 * 6   0 0 0 1 0 0 0 0                                                        *
 * 5   0 0 0 1 0 0 0 0                                                        *
 * 4   1 1 1 X 1 1 1 1                                                        *
 * 3   0 0 0 1 0 0 0 0                                                        *
 * 2   0 0 0 1 0 0 0 0                                                        *
 * 1   0 0 0 1 0 0 0 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard rook_mask(int rank, int file)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);

    Bitboard bb = 0;
    int n_rank = rank+1;
    while(n_rank < 7)
    {
        bb ^= (Bitboard)1<<(8*n_rank + file);
        ++n_rank;
    }

    n_rank = rank-1;
    while(n_rank > 0)
    {
        bb ^= (Bitboard)1<<(8*n_rank + file);
        --n_rank;
    }

    int n_file = file+1;
    while(n_file < 7)
    {
        bb ^= (Bitboard)1<<(8*rank + n_file);
        ++n_file;
    }

    n_file = file-1;
    while(n_file > 0)
    {
        bb ^= (Bitboard)1<<(8*rank + n_file);
        --n_file;
    }

    return bb;
}

/******************************************************************************
 * Bishop mask, includes edges                                                *
 *                                                                            *
 * 8   0 0 0 0 0 0 0 1                                                        *
 * 7   1 0 0 0 0 0 1 0                                                        *
 * 6   0 1 0 0 0 1 0 0                                                        *
 * 5   0 0 1 0 1 0 0 0                                                        *
 * 4   0 0 0 X 0 0 0 0                                                        *
 * 3   0 0 1 0 1 0 0 0                                                        *
 * 2   0 1 0 0 0 1 0 0                                                        *
 * 1   1 0 0 0 0 0 1 0                                                        *
 *                                                                            *
 *     A B C D E F G H                                                        *
 ******************************************************************************/
Bitboard bishop_mask(int rank, int file)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);

    Bitboard bb = 0;
    int n_rank = rank+1;
    int n_file = file+1;
    while(n_rank < 7 && n_file < 7)
    {
        bb ^= (Bitboard)1<<(8*n_rank + n_file);
        ++n_rank; ++n_file;
    }

    n_rank = rank+1;
    n_file = file-1;
    while(n_rank < 7 && n_file > 0)
    {
        bb ^= (Bitboard)1<<(8*n_rank + n_file);
        ++n_rank; --n_file;
    }

    n_rank = rank-1;
    n_file = file+1;
    while(n_rank > 0 && n_file < 7)
    {
        bb ^= (Bitboard)1<<(8*n_rank + n_file);
        --n_rank; ++n_file;
    }

    n_rank = rank-1;
    n_file = file-1;
    while(n_rank > 0 && n_file > 0)
    {
        bb ^= (Bitboard)1<<(8*n_rank + n_file);
        --n_rank; --n_file;
    }

    return bb;
}

/******************************************************************************
 * Vector of all bitboards with bitcount of 2                                 *
 ******************************************************************************/
void pop2_bb(vector<Bitboard> &bbs)
{
	int i, j, index = 0;
	bbs.resize(2016);

	for(i = A1; i <= H8; ++i)
	{
		for(j = i+1; j <= H8; ++j)
		{
			bbs[index] = (Bitboard)(1<<i) | (Bitboard)(1<<j);
			++index;
		}
	}
}

inline Bitboard pick_magic()
{
    return genrand64_int64() & genrand64_int64();
}

/******************************************************************************
 * Draws bitboard in chess board pattern                                      *
 ******************************************************************************/
void draw_bitboard(Bitboard bb, int highlight=-1)
{
    Bitboard bb_cp = bb, bit, i;
    int rank, file;

    for(rank=7; rank >= 0; --rank)
    {
        cout << rank + 1 << " ";

        for(file=0; file < 8; ++file)
        {
            i = rank*8 + file;
            bit = (Bitboard)1<<i;
            if(i == highlight)
            {
                cout << "X ";
            } else if((bb & bit))
            {
                cout << "1 ";
            }
            else
            {
                cout << "0 ";
            }
        }
        cout << endl;
    }
    cout << "  A B C D E F G H " << endl;
}

void draw_board(Board* board)
{
    
}

/******************************************************************************
 * Populates magic[] with the appropriate magic numbers for each square       *
 * Populates result[]                                                         *
 ******************************************************************************/
void init_sliding_moves(Bitboard magic[], Bitboard* usedBy[], vector<vector<Bitboard> > &attack, vector<vector<Bitboard> > &occupancyMaskVariations, int shift[])
{
    int i, index, key;
	size_t maxKey;

    for(i = A1; i <= H8; ++i)
    {
        maxKey = attack[i].size();
		usedBy[i] = new Bitboard[maxKey];

        do
        {
            magic[i] = pick_magic();
			memset(usedBy[i], 0, maxKey*sizeof(Bitboard));

            for (key = 0; key < maxKey; key++)
            {
                index = (int)((occupancyMaskVariations[i][key]*magic[i]) >> shift[i]);

                if (usedBy[i][index] == 0)
                    usedBy[i][index] = attack[i][key];
                else if (usedBy[i][index] != attack[i][key])
                    break;
            }
        } while (key != maxKey);
    }
}

/******************************************************************************
 * Initializes bitboard databases (Moves::*)                                  *
 ******************************************************************************/
void init_moves()
{
    int i, side, rank, file;

    for(i=A1; i<=H8; ++i)
    {
        rank = i/8;
        file = i%8;
        Moves::King[i] = king_moves(rank, file);
        Moves::Knight[i] = knight_moves(rank, file);
        Moves::RookMask[i] = rook_mask(rank, file);
        Moves::BishopMask[i] = bishop_mask(rank, file);
        Moves::RookShifts[i] = 64 - popcnt(Moves::RookMask[i]);
        Moves::BishopShifts[i] = 64 - popcnt(Moves::BishopMask[i]);

        for(side=0; side<2; ++side)
        {
            Moves::PawnSingle[side][i] = pawn_single_moves(rank, file, side);
            Moves::PawnDouble[side][i] = pawn_double_moves(rank, file, side);
            Moves::PawnCapture[side][i] = pawn_capture_moves(rank, file, side);
        }

        bit_variations(Moves::RookMaskVariations[i], Moves::RookMask[i]);
        attack_sets(Moves::RookAttackSet[i], Moves::RookMaskVariations[i], i, true);

        bit_variations(Moves::BishopMaskVariations[i], Moves::BishopMask[i]);
        attack_sets(Moves::BishopAttackSet[i], Moves::BishopMaskVariations[i], i, false);
    }
	
	init_sliding_moves(Moves::BishopMult, Moves::BishopAttacks, Moves::BishopAttackSet, Moves::BishopMaskVariations, Moves::BishopShifts);
	init_sliding_moves(Moves::RookMult, Moves::RookAttacks, Moves::RookAttackSet, Moves::RookMaskVariations, Moves::RookShifts);
}

/******************************************************************************
 * Converts piece char to piece                                               *
 ******************************************************************************/
int piece_char_to_piece(char name)
{
    assert(isalpha(name));
    switch(toupper(name))
    {
        case 'P': return P;
        case 'B': return B;
        case 'N': return N;
        case 'R': return R;
        case 'Q': return Q;
        case 'K': return K;
        default: return NULL;
    }
}

/******************************************************************************
 * Converts file char (a-h) to file (0-7)                                     *
 ******************************************************************************/
int file_char_to_file(char filechar)
{
    assert(isalpha(filechar));
    assert(filechar >= 'a' && filechar <= 'h');
    return filechar - 'a';
}

/******************************************************************************
 * Converts rank and file (0-7) to index (0-63)                               *
 ******************************************************************************/
inline int rank_file_to_index(int rank, int file)
{
    assert(rank<8 && rank>=0);
    assert(file<8 && file>=0);
    return 8*rank + file;
}

/******************************************************************************
 * Initializes and returns pointer to a Board structure created from a FEN    *
 * board string. TODO: guarantee Board values initialize to 0                 *
 ******************************************************************************/
Board* init_board(string fen = FEN::StartPos)
{
    Board* board = new Board;
    size_t fenlen = fen.length();
    int i, turn, rank = 7, file = 0, piece;

    /* Piece layout section */
    for(i = 0; fen[i] != ' ' && rank >= 0; ++i)
    {
        if(isdigit(fen[i]))
        {
            file += (fen[i] - '0');
            continue;
        }

        if(fen[i] == '/')
        {
            file = 0;
            rank -= 1;
            continue;
        }

        turn = isupper(fen[i]) ? White : Black;
        piece = piece_char_to_piece(fen[i]);
        setbit<Bitboard>(board->colour[turn].pieces[piece], rank_file_to_index(rank, file));
        ++file;
    }

    /* Turn section */
    ++i;
    board->turn = (fen[i] == 'w' ? White : Black);

    /* Castling section */
    i += 2;

    for(; fen[i] != ' '; ++i)
    {
        switch(fen[i])
        {
            case 'K':
                board->flags |= Flags::WShort;
                break;
            case 'Q':
                board->flags |= Flags::WLong;
                break;
            case 'k':
                board->flags |= Flags::BShort;
                break;
            case 'q':
                board->flags |= Flags::BLong;
                break;
            default: break;
        }
    }

    /* En passant section */
    ++i;
    if(fen[i] != '-')
    {
        file = file_char_to_file(fen[i]);
        ++i;
        rank = fen[i] - '1'; /* convert to rank (0-7) */
        setbit<Bitboard>(board->ep_square, rank_file_to_index(rank, file));
    }

    /* Half move/full move section */
    /* TODO */
    //i += 2;

    return board;
}

void add(Move move)
{
}

void subtract(Move move)
{
}

/******************************************************************************
 * Populates a move list                                                      *
 *                                                                            *
 * TODO                                                                       *
 *   Extract moves into Move format                                           *
 *   Optimization: add flag to disable Move creation at depth=1               *
 *   -> Instead only return count                                             *
 *   Optimization: add flag to disable Move creation at depth=1.              *
 *   -> Instead only return count                                             *
 ******************************************************************************/
unsigned gen(Board* board, Move* moves)
{
    Colour* me = &board->colour[board->turn];
    Colour* enemy = &board->colour[board->turn^1];

    /* Important and useful bitboards */
    Bitboard enemy_bb = enemy->pieces[P] | enemy->pieces[B] | enemy->pieces[N] | enemy->pieces[R] | enemy->pieces[Q] | enemy->pieces[K];
    Bitboard me_bb = me->pieces[P] | me->pieces[B] | me->pieces[N] | me->pieces[R] | me->pieces[Q] | me->pieces[K];
    Bitboard occupied_bb = enemy_bb | me_bb;
    Bitboard empty_bb = ~occupied_bb;
    Bitboard enemy_and_empty_bb = enemy_bb | empty_bb;

    Bitboard bb, move_bb;
    int i;

    /* Pawn, Single */
    bb = me->pieces[P];
    while(bb > 0)
    {
        i = bsf(bb);
                                                  /* KING BB */
        move_bb = Moves::PawnSingle[board->turn][i] & enemy_bb;
                                                  /* KING BB */
        move_bb |= Moves::PawnDouble[board->turn][i] & enemy_bb;

        bb = bb^(Bitboard)1<<i;                   /* note: no need to do this on 8th iteration */
    }

    /* King */
    bb = me->pieces[K];
    i = bsr(bb);
    move_bb = Moves::King[i] & enemy_and_empty_bb;/* KING BB */

    /* Knight */
    bb = me->pieces[N];
    while(bb > 0)
    {
        i = bsr(bb);
                                                  /* KNIGHT BB */
        move_bb = Moves::Knight[i] & enemy_and_empty_bb;
        bb = bb ^ (Bitboard)1<<i;                 /* note: no need to do this on second iteration */
    }

    /* Bishop */
    /* Rook */
    /* Queen */

    unsigned count = 0;
    return count;
}
/*
int main()
{
    clock_t start, end;
    double timespan;
    for(int i = 0; i < 30; ++i)
    {
        init_board(FEN::Benchmarks[i]);
    }
    //init_moves();
	Bitboard bitboard;

    start = clock();
	for(int j = 0; j < 100; ++j)
	{
		for(int i = 0; i < 64; ++i)
		{
			bitboard = 0;
			setbit<Bitboard>(bitboard, i);
			bsr(bitboard);
		}
	}
    end = clock();
    timespan = (double)(end - start) / CLOCKS_PER_SEC;
    printf("That took DOUBLE FOREVER. %.6fs\n", timespan);

    return 0;
}
*/
