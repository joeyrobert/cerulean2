//#define CMP_INTRINSICS

#ifdef _MSC_VER
#  define U64(u) (u##ui64)
#else
#  define U64(u) (u##ULL)
#endif

#include <stdint.h>
#include <cstdio>
#include <ctime>
#include <cassert>
#include <iostream>
#include <memory>
#include <string>
#include <cstring>
#include <vector>
#include <cctype>

using namespace std;

#ifdef CMP_INTRINSICS
#   include <intrin.h>
#endif

#ifndef CENEE_H
#define CENEE_H

typedef uint64_t Bitboard;
typedef uint64_t Zobrist;
typedef uint64_t Perft;
typedef uint16_t Move;

#include "transposition.h"
#include "mersenne.h"

/******************************************************************************
 * Turn enumeration                                                           *
 ******************************************************************************/
enum Turn
{
    White = 0,
    Black = 1,
};

/******************************************************************************
 * Piece enumeration (used in make/unmake)                                    *
 ******************************************************************************/
enum Piece
{
    P = 0,
    B,
    N,
    R,
    Q,
    K
};

/******************************************************************************
 * Square enumeration                                                         *
 ******************************************************************************/
enum Square
{
    A1 = 0, B1, C1, D1, E1, F1, G1, H1,
    A2, B2, C2, D2, E2, F2, G2, H2,
    A3, B3, C3, D3, E3, F3, G3, H3,
    A4, B4, C4, D4, E4, F4, G4, H4,
    A5, B5, C5, D5, E5, F5, G5, H5,
    A6, B6, C6, D6, E6, F6, G6, H6,
    A7, B7, C7, D7, E7, F7, G7, H7,
    A8, B8, C8, D8, E8, F8, G8, H8
};

/******************************************************************************
 * Bitboards for one Colour                                                   *
 * Two of these maintained simultaneously (white, black)                      *
 *                                                                            *
 * MSB                                                                 LSB    *
 * 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000    *
 * 8        7        6        5        4        3        2        1           *
 * HGFEDCBA HGFEDCBA HGFEDCBA HGFEDCBA HGFEDCBA HGFEDCBA HGFEDCBA HGFEDCBA    *
 *                                                                            *
 *                  <- bit             bit ->                                 *
 * MSB > H8G8F8E8D8C8B8A8 ^          ^ A8B8C8D8E8F8G8H8 < MSB                 *
 *       H7G7F7E7D7C7B7A7 |          | A7B7C7D7E7F7G7H7                       *
 *       H6G6F6E6D6C6B6A6 b          b A6B6C6D6E6F6G6H6                       *
 *       H5G5F5E5D5C5B5A5 y          y A5B5C5D5E5F5G5H5                       *
 *       H4G4F4E4D4C4B4A4 t          t A4B4C4D4E4F4G4H4                       *
 *       H3G3F3E3D3C3B3A3 e          e A3B3C3D3E3F3G3H3                       *
 *       H2G2F2E2D3C2B2A2              A2B2C2D2E2F2G2H2                       *
 *       H1G1F1E1D1C1B1A1 < LSB  LSB > A1B1C1D1E1F1G1H1                       *
 ******************************************************************************/
typedef struct
{
    Bitboard pieces[6];
} Colour;

/******************************************************************************
 * Transposition table entry                                                  *
 ******************************************************************************/
typedef struct
{
    Zobrist key;
    Perft perft;
} perft_t;

/******************************************************************************
 * Board                                                                      *
 * key - Zobrist of board                                                     *
 * turn - 0 for white, 1 for black                                            *
 * colour - Bitboards, indexed by turn ([0] for white, [1] for black)         *
 * ep_square - en passant target square                                       *
 * flags - castling (KQkq)                                                     *
 ******************************************************************************/
typedef struct
{
    Zobrist key;
    Bitboard ep_square;
    Colour colour[2];
    int turn;
    int flags;
    int halfmove;
    int fullmove;
} Board;

namespace Flags
{
    const int WShort = 1;
    const int WLong = 2;
    const int BShort = 4;
    const int BLong = 8;
}

/******************************************************************************
 * Moves::*                                                                   *
 * Static lookup arrays used in move generation                               *
 ******************************************************************************/
namespace Moves
{
    const Bitboard edges = U64(0xff818181818181ff);

    Bitboard Knight[64];
    Bitboard King[64];

    /* Pawn moves depend on side moving */
    Bitboard PawnSingle[2][64];
    Bitboard PawnDouble[2][64];
    Bitboard PawnCapture[2][64];

    /* Sliding pieces */
    Bitboard RookMask[64];
    Bitboard BishopMask[64];
    int RookShifts[64];
    int BishopShifts[64];
    Bitboard BishopMult[64];
    Bitboard RookMult[64];
    Bitboard *BishopAttacks[64];
    Bitboard *RookAttacks[64];

    /* Occupancy/Attack set variables used to make magic numbers */
    vector<vector<Bitboard> > RookMaskVariations(64);
    vector<vector<Bitboard> > BishopMaskVariations(64);
    vector<vector<Bitboard> > RookAttackSet(64);
    vector<vector<Bitboard> > BishopAttackSet(64);

	/* MAGICAL numbers! */
    vector<Bitboard> Pop2BB(2016);
	Bitboard Pop2Magic;
	int Pop2BBBitLoc[2016][2];
}

/******************************************************************************
 * Lookup array for de Bruijn implementation of Bitscan Forward               *
 ******************************************************************************/
const int bsf_index[64] =
{
    63,  0, 58,  1, 59, 47, 53,  2,
    60, 39, 48, 27, 54, 33, 42,  3,
    61, 51, 37, 40, 49, 18, 28, 20,
    55, 30, 34, 11, 43, 14, 22,  4,
    62, 57, 46, 52, 38, 26, 32, 41,
    50, 36, 17, 19, 29, 10, 13, 21,
    56, 45, 25, 31, 35, 16,  9, 12,
    44, 24, 15,  8, 23,  7,  6,  5
};
const uint64_t bsf_magic = U64(0x07EDD5E59A4E28C2);

/******************************************************************************
 * FEN boards                                                                 *
 ******************************************************************************/
namespace FEN
{
    const string StartPos("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
    const string Benchmarks[30] = {
        "r4rk1/ppp2p2/1bnpbq1p/4p1p1/2B1Pn2/2PP2B1/PPNNQPPP/R4RK1 w - - 0 14",
        "1r6/1p1n4/3p2r1/pP2p2k/P3Pp1p/3N1P1P/R5P1/2R4K w - - 0 36",
        "7R/3k4/3p4/4p3/Pr2Pp1p/5P1P/6PK/8 w - - 0 56",
        "2r2rk1/1bq1npp1/p1n1p2p/1pp1P3/4B3/P1N2N2/1PP1QPPP/3RR1K1 b - - 0 15",
        "7k/2qr1pp1/p3p2p/n3P3/P1p3Q1/1pPnNN1P/1P3PP1/1R4K1 w - - 0 33",
        "7k/5Qp1/pq2n2p/n2rPN2/2p3N1/1pP4P/1P4PK/1R6 b - - 0 46",
        "rn1r2k1/1p3p1p/1p1b2p1/2ppN3/3PnP2/P1PR3B/1P4PP/R1B3K1 w - - 0 19",
        "1r6/5pk1/8/2N4p/3p1Pp1/2p1n1P1/4B2P/2R3K1 w - - 0 41",
        "rr4k1/2q1bpp1/2ppn2p/p3p3/PPQ1P3/2P1BN1P/5PP1/R2R2K1 w - - 0 21",
        "3r2k1/4qpp1/PP3n1p/1QN1p3/2P1P3/7P/5PP1/6K1 w - - 0 37",
        "r1b1k2r/1pqpb1pp/p1n1pn2/5p2/1PP5/P1N1PN2/1B3PPP/R2QKB1R w KQkq - 0 10",
        "1r4k1/1p5p/4pq1r/1PPp1p1b/R7/1N2P1P1/3Q3P/2R3K1 b - - 0 37",
        "4b1k1/2q5/4R3/3p1p2/4r3/4PNP1/5K2/Q7 w - - 0 52",
        "r4rk1/pp1bppbp/1q1p1np1/4n3/2PNP3/2N1BP2/PP1QB1PP/R4RK1 b - - 0 12",
        "4r3/1pqbrpkp/3p1pp1/4n3/pPPNP3/P4P1P/3Q2P1/2R1RB1K b - - 0 36",
        "2r5/7p/1p5k/1P3pp1/2P1bP2/P3R1KP/6P1/5B2 b - - 0 61",
        "8/8/PR6/1P2k3/2P5/7r/2K5/8 b - - 0 80",
        "3rqbk1/1p4p1/r3ppNp/3n4/1nNB1P2/4P1Q1/1R4PP/1R4K1 b - - 0 28",
        "r3r1k1/pp2ppbp/2b1n1p1/q1pNn3/2P1P3/1P2B2P/P1Q1BPP1/1R1RN1K1 b - - 0 18",
        "8/7p/p1pk2p1/4pp2/2P2n1P/1PN2PP1/P4K2/8 b - - 0 44",
        "8/8/p3pk1p/5pp1/2PK4/1P4P1/P7/8 w - - 0 0",
        "r5k1/q2b1ppp/2p2n2/Pr1p4/1P1Rp3/1NQ1P1P1/5PBP/R5K1 b - - 0 25",
        "r1bqk2r/pp2bppp/2n5/3pp2n/1P6/2PQBNP1/P3PPBP/RN3RK1 w kq - 0 12",
        "5k2/8/5pb1/8/3n1KBR/7P/8/8 b - - 0 73",
        "r2q1rk1/2p1npp1/1pbppn1p/2P5/P2P4/4PN1P/PB2BPP1/R2Q1RK1 w - - 0 15",
        "8/r4pk1/4p2p/2Np2p1/3P2P1/1R1BP1KP/nn1r1P2/R7 w - - 0 45",
        "3q1rk1/p1rnppbp/Bpbp2p1/2p5/3P4/2P1P2P/PP1NQPPB/2R2RK1 w - - 0 16",
        "3r2k1/p4q2/2R2pp1/3n4/3B4/r1P2Q2/6P1/5RK1 w - - 0 42",
        "2r2rk1/pp1nbpp1/2p1pn1p/q1Pp1b2/3P4/1P1N2P1/PB1NPPBP/R2Q1RK1 w - - 0 13",
        "1r1q2k1/pP3p2/B2np3/3p4/3P4/6P1/1Q4KP/2R5 w - - 0 39"
    };
}

/******************************************************************************
 * Function prototypes                                                        *
 ******************************************************************************/
template <class INTTYPE> inline void setbit(INTTYPE &value, int index);
inline int popcnt(uint64_t x);
inline int bsr(uint64_t x);
inline int bsf(uint64_t x);
void on_bits(vector<int> &on_bits_index, Bitboard x);
void bit_variations(vector<Bitboard> &bit_var, Bitboard x);
void attack_sets(vector<Bitboard> &attset, vector<Bitboard> &bitvar, int index, bool is_rook);
Bitboard king_moves(int rank, int file);
Bitboard knight_moves(int rank, int file);
Bitboard pawn_single_moves(int rank, int file, int side);
Bitboard pawn_double_moves(int rank, int file, int side);
Bitboard pawn_capture_moves(int rank, int file, int side);
Bitboard rook_mask(int rank, int file);
Bitboard bishop_mask(int rank, int file);
void pop2_bb(vector<Bitboard> &bbs);
inline Bitboard pick_magic();
void draw_bitboard(Bitboard bb, int highlight);
void init_sliding_moves(Bitboard magic[], Bitboard* usedBy[], vector<vector<Bitboard> > &attack, vector<vector<Bitboard> > &occupancyMaskVariations, int shift[]);
void init_moves();
Board* init_board(string fen);
void add(Move move);
void subtract(Move move);
unsigned gen(Board* board, Move* moves);

#endif
