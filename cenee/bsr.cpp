#include <cstdio>
#include <cstring>
#include <stdint.h>
#include "mersenne.h"
#include <intrin.h>

inline int bsr(uint64_t x)
{
    int result = 0;
    if(x > 0xFFFFFFFF)                            /* 2**32-1 */
    {
        x >>= 32;
        result = 32;
    }
    if(x > 0xFFFF)                                /* 2**16-1 */
    {
        x >>= 16;
        result += 16;
    }
    if(x > 0xFF)                                  /* 2**8-1 */
    {
        x >>= 8;
        result += 8;
    }
    if(x > 0xF)                                   /* 2**4-1 */
    {
        x >>= 4;
        result += 4;
    }
    if(x > 3)                                     /* 2**2-1 */
    {
        x >>= 2;
        result += 2;
    }
    if(x > 1)                                     /* 2**1-1 */
    {
        x >>= 1;
        result += 1;
    }
    return result;
}

inline uint64_t pick_magic()
{
    return genrand64_int64() & genrand64_int64();
}

void init_bsr(int bits)
{
    int index, bsr_index;
    uint64_t maxKey, key, size=1<<bits;
    uint64_t *usedBy, *magic = new uint64_t[size];

    for(int i = 0; i < size; ++i)
    {
        maxKey = (uint64_t)1<<i;
		usedBy = new uint64_t[maxKey];

        do
        {
            magic[i] = pick_magic();
			memset(usedBy, 0, maxKey*sizeof(uint64_t));

            for (key = 0; key < maxKey; ++key) /* need to ensure each one has same index */
            {
                index = (int)((key*magic[i]) >> (64-bits)); /* 64-5 for 2^5=32 */
				bsr_index = bsr(key);
                if (usedBy[index] == 0)
                    usedBy[index] = bsr_index; 
                else if (usedBy[index] != bsr_index)
                    break;
            }
        } while (key != maxKey);

        printf("Magic number for keys < 2^%d: %llu\n", i, magic[i]);
        delete[] usedBy;
    }

    delete[] magic;
}

int main()
{
    init_bsr(5); /* optimal is 5 bits */
    return 0;
}
