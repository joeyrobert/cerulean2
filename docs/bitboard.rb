def king_moves(rank, file)
	bb = 0
	bb ^= 1 << (8*(rank+1) + file) if rank+1<8 
	bb ^= 1 << (8*(rank-1) + file) if rank-1>=0 
	bb ^= 1 << (8*rank + file+1) if file+1<8
	bb ^= 1 << (8*rank + file-1) if file-1>=0
	bb ^= 1 << (8*(rank+1) + file+1) if rank+1<8 && file+1<8
	bb ^= 1 << (8*(rank+1) + file-1) if rank+1<8 && file-1>=0
	bb ^= 1 << (8*(rank-1) + file+1) if rank-1>=0 && file+1>8
	bb ^= 1 << (8*(rank-1) + file-1) if rank-1>=0 && file-1>=0
	return bb
end

def knight_moves(rank, file)
	bb = 0
	bb ^= 1 << (8*(rank+2) + file+1) if rank+2<8 && file+1<8
	bb ^= 1 << (8*(rank+2) + file-1) if rank+2<8 && file-1>=0
	bb ^= 1 << (8*(rank-2) + file+1) if rank-2>=0 && file+1<8
	bb ^= 1 << (8*(rank-2) + file-1) if rank-2>=0 && file-1>=0
	bb ^= 1 << (8*(rank+1) + file+2) if rank+1<8 && file+2<8
	bb ^= 1 << (8*(rank-1) + file+2) if rank-1>=0 && file+2<8
	bb ^= 1 << (8*(rank+1) + file-2) if rank+1<8 && file-2>=0
	bb ^= 1 << (8*(rank-1) + file-2) if rank-1>=0 && file-2>=0
	return bb
end

8.times do |rank|
    8.times do |file|
        #puts "%064b" % (1 << (8*rank + file))
        bb = knight_moves(rank, file)

        puts bb
		#puts ""
    end
end